import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';

//Do not spam, limited to 1000 calls per day
//These are used in other API files
var APIlink = 'https://www.omdbapi.com/';
var APIkey = '?apikey=3d28842a';

//Used by APISearch to pull specific show
var APIDirectName = "&i=tt0903747";
var APIshowDirect = APIlink + APIkey + APIDirectName;


//For testing, just to use search bar in search page
var APIDirectNameSearch = "&t=Breaking+Bad";

//Pings the sever for the data and gets the response as a json
//Returns the json string
Future<ShowData> getShowDirect() async {
  final response = await http.get(APIshowDirect);
  return postFromJson(response.body);
}

//Takes the json string, puts it onto, and returns the info
ShowData postFromJson(String str) {
  final jsonData = json.decode(str);
  return ShowData.fromJson(jsonData);
}

//Takes the data to convert into json
//Used for sending data to the server
String postToJson(ShowData data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

//Class to hold all the data in it's own variable
class ShowData {
  //Parse the json into strings/list
  //Empty data shows up as "null"
  ShowData({
    this.title,
    this.year,
    this.rated,
    this.released,
    this.runtime,
    this.genre,
    this.director,
    this.writer,
    this.actors,
    this.plot,
    this.language,
    this.country,
    this.awards,
    this.poster,
    this.ratings,
    this.metascore,
    this.imdbRating,
    this.imdbVotes,
    this.imdbId,
    this.type,
    this.totalSeasons,
    this.response,
  });

  //variables that hold data
  String title;
  String year;
  String rated;
  String released;
  String runtime;
  String genre;
  String director;
  String writer;
  String actors;
  String plot;
  String language;
  String country;
  String awards;
  String poster;
  List<Rating>
      ratings; //Sometimes has more them 1 rating, so it is kept as a list
  String metascore;
  String imdbRating;
  String imdbVotes;
  String imdbId;
  String type;
  String totalSeasons;
  String response;

  factory ShowData.fromJson(Map<String, dynamic> json) => ShowData(
        title: json["Title"],
        year: json["Year"],
        rated: json["Rated"],
        released: json["Released"],
        runtime: json["Runtime"],
        genre: json["Genre"],
        director: json["Director"],
        writer: json["Writer"],
        actors: json["Actors"],
        plot: json["Plot"],
        language: json["Language"],
        country: json["Country"],
        awards: json["Awards"],
        poster: json["Poster"],
        ratings:
            List<Rating>.from(json["Ratings"].map((x) => Rating.fromJson(x))),
        metascore: json["Metascore"],
        imdbRating: json["imdbRating"],
        imdbVotes: json["imdbVotes"],
        imdbId: json["imdbID"],
        type: json["Type"],
        totalSeasons: json["totalSeasons"],
        response: json["Response"],
      );

  //For converting into json
  Map<String, dynamic> toJson() => {
        "Title": title,
        "Year": year,
        "Rated": rated,
        "Released": released,
        "Runtime": runtime,
        "Genre": genre,
        "Director": director,
        "Writer": writer,
        "Actors": actors,
        "Plot": plot,
        "Language": language,
        "Country": country,
        "Awards": awards,
        "Poster": poster,
        "Ratings": List<dynamic>.from(ratings.map((x) => x.toJson())),
        "Metascore": metascore,
        "imdbRating": imdbRating,
        "imdbVotes": imdbVotes,
        "imdbID": imdbId,
        "Type": type,
        "totalSeasons": totalSeasons,
        "Response": response,
      };
}

//Class that holds the ratings from other sites
class Rating {
  Rating({
    this.source,
    this.value,
  });

  String source;
  String value;

  factory Rating.fromJson(Map<String, dynamic> json) => Rating(
        source: json["Source"],
        value: json["Value"],
      );

  Map<String, dynamic> toJson() => {
        "Source": source,
        "Value": value,
      };
}
