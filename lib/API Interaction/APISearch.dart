import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'APIDirect.dart';

//Do not spam, limited to 1000 calls per day
var APIsearchName = "&s=";
var APIshowSearch = APIlink + APIkey + APIsearchName;

//Pings the sever for the data and gets the response as a json
//Returns the json string
Future<ShowDataSearch> getShowSearch() async {
  final response = await http.get(APIshowSearch);
  return postFromJsonSearch(response.body);
}

//Takes the json string, puts it onto, and returns the info
ShowDataSearch postFromJsonSearch(String str) {
  final jsonData = json.decode(str);
  return ShowDataSearch.fromJson(jsonData);
}

class ShowDataSearch {
  ShowDataSearch({
    this.search,
    this.totalResults,
    this.response,
  });

  List<Search> search;
  String totalResults;
  String response;

  factory ShowDataSearch.fromJson(Map<String, dynamic> json) => ShowDataSearch(
    search: List<Search>.from(json["Search"].map((x) => Search.fromJson(x))),
    totalResults: json["totalResults"],
    response: json["Response"],
  );

  Map<String, dynamic> toJson() => {
    "Search": List<dynamic>.from(search.map((x) => x.toJson())),
    "totalResults": totalResults,
    "Response": response,
  };
}

class Search {
  Search({
    this.title,
    this.year,
    this.imdbId,
    this.type,
    this.poster,
  });

  String title;
  String year;
  String imdbId;
  String type;
  String poster;

  factory Search.fromJson(Map<String, dynamic> json) => Search(
    title: json["Title"],
    year: json["Year"],
    imdbId: json["imdbID"],
    type: json["Type"],
    poster: json["Poster"],
  );

  Map<String, dynamic> toJson() => {
    "Title": title,
    "Year": year,
    "imdbID": imdbId,
    "Type": typeValues.reverse[type],
    "Poster": poster,
  };
}

enum Type { MOVIE, SERIES }

final typeValues = EnumValues({
  "movie": Type.MOVIE,
  "series": Type.SERIES
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
