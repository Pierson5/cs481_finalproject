import 'package:flutter/material.dart';
import 'APIDirect.dart';

class APIDirectPageTest extends StatefulWidget {
  @override
  _APIDirectPageTestState createState() => _APIDirectPageTestState();
}

class _APIDirectPageTestState extends State<APIDirectPageTest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder<ShowData>(
            future: getShowDirect(), //Assign the future used, data goes to snapshot
            builder: (context, snapshot) {
              //Check if the connection is done
              if (snapshot.connectionState == ConnectionState.done) {
                //Display error if something goes wrong
                if (snapshot.hasError) {
                  return Text("Error");
                }
                //Display the data from the server
                return ListView(
                  children: [
                    //Input to pull up title info. Directly goes to the found show
                    //Only here to test calling API directly
                    TextField(
                      onSubmitted: (String userInput) {
                        //Replace all user " " with "+" for the API
                        String parseInput = userInput.replaceAll(RegExp(' '), '+');
                        setState(() {
                          APIDirectNameSearch = "&t=" + parseInput;
                          APIshowDirect = APIlink + APIkey + APIDirectNameSearch;
                        });
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Search Show',
                      ),
                    ),
                    Center(
                      child: Text('Show Name : ${snapshot.data.title}\n'
                          'Year : ${snapshot.data.year}\n'
                          'Seasons : ${snapshot.data.totalSeasons}\n'),
                    ),
                    Image.network(snapshot.data.poster),
                  ],
                );
              } else
                //If connection is not done, just show loading circle
                return CircularProgressIndicator();
            }));
  }
}
