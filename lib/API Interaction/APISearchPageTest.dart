import 'package:cs481_finalproject/API%20Interaction/APIDirectPageTest.dart';
import 'package:flutter/material.dart';
import 'APISearch.dart';
import 'APIDirect.dart';

class APISearchPageTest extends StatefulWidget {
  @override
  _APISearchPageTestState createState() => _APISearchPageTestState();
}

class _APISearchPageTestState extends State<APISearchPageTest> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder<ShowDataSearch>(
            future: getShowSearch(),
            //Assign the future used, data goes to snapshot
            builder: (context, searchData) {
              //Check if the connection is done
              if (searchData.connectionState == ConnectionState.done) {
                //Display error if something goes wrong
                if (searchData.hasError) {
                  return ListView(
                    children: [
                      TextField(
                        onSubmitted: (String userInput) {
                          //Replace all user " " with "+" for the API
                          String parseInput =
                          userInput.replaceAll(RegExp(' '), '+');
                          setState(() {
                            APIsearchName = "&s=" + parseInput;
                            APIshowSearch =
                                APIlink + APIkey + APIsearchName;
                          });
                        },
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Search Show',
                        ),
                      ),
                      Text("Error"),
                    ],
                  );
                }
                List<Search> searchInfo = searchData.data.search;
                //Display the data from the server
                return ListView(children: [
                  TextField(
                    onSubmitted: (String userInput) {
                      //Replace all user " " with "+" for the API
                      String parseInput =
                          userInput.replaceAll(RegExp(' '), '+');
                      setState(() {
                        APIsearchName = "&s=" + parseInput;
                        APIshowSearch =
                            APIlink + APIkey + APIsearchName;
                      });
                    },
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Search Show',
                    ),
                  ),
                  //Loop to make a card for each search result
                  for (var i in searchInfo) Card(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Row(
                          children: [
                            Image.network(
                              i.poster,
                              height: 150,
                              width: 120,
                            ),
                            Column(
                              children: [
                                Container(
                                  width: 200,
                                  child: Text(
                                    i.title,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                    ),
                                  ),
                                ),
                                Text(
                                  i.year,
                                  style: TextStyle(
                                    fontStyle: FontStyle.italic,
                                    fontSize: 13,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            const SizedBox(width: 8),
                            //Button to make a page for the chosen show
                            TextButton(
                              child: const Text('MORE INFO'),
                              onPressed: () {
                                setState(() {
                                  APIDirectName = "&i=" + i.imdbId;
                                  APIshowDirect = APIlink + APIkey + APIDirectName;
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => APIDirectPageTest()),
                                  );
                                });
                              },
                            ),
                            const SizedBox(width: 8),
                          ],
                        ),
                      ],
                    ),
                  ),
                ]);
              } else
                //If connection is not done, just show loading circle
                return CircularProgressIndicator();
            }));
  }
}
