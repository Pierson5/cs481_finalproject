import 'dart:io';
import 'package:intl/intl.dart';

//Show class
class Show {

  String showTitle;
  int totalEpisodes;
  int currentEpisode;
  File showImage;
  String networkShowImage;
  String showDesc;
  String showDate;
  bool manualAdd;

  //global showList
  static var showList = new List<Show>();

  Show({this.showTitle, this.totalEpisodes, this.currentEpisode, this.showImage, this.showDesc, this.showDate, this.manualAdd});

  //function to add a new show
  void addShow(newTitle, newEps, newCurEp, newImg, newDesc, newDate, newAdd)
  {

    showList.add(new Show(showTitle: newTitle, totalEpisodes: newEps, currentEpisode: newCurEp, showImage: newImg, showDesc: newDesc, showDate: newDate, manualAdd: newAdd));

  }

  //function to get list of Shows
  List<Show> getShows()
  {

    return showList;
  }

  //function to update a show's title
  void updateTitle(pos, newTitle)
  {
    showList[pos].showTitle = newTitle;
  }

  //function to update a show's image
  void updateShowImg(pos, newShowImg)
  {
    showList[pos].showImage = newShowImg;
  }

  //function to update a show's current episode, also updates the date
  void updateShowCurr(pos, newShowCurr)
  {
    showList[pos].currentEpisode = newShowCurr;
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('MMM d, y').format(now);
    showList[pos].showDate = formattedDate;
  }


  //function to update a show's total episodes
  void updateShowEp(pos, newShowEp)
  {
    showList[pos].totalEpisodes = newShowEp;
  }

  //function to update a show's description
  void updateShowDesc(pos, newShowDesc)
  {
    showList[pos].showDesc = newShowDesc;
  }

  int getTotalShows()
  {
    return showList.length;
  }

  int getTotalEpWatched()
  {
    var total = 0;
    for(var show in showList)
      total = total + show.currentEpisode;
    return total;
  }

  int getTotalEpUnwatched()
  {
    var watched = getTotalEpWatched();
    var total = 0;
    for(var show in showList)
      total = total + show.totalEpisodes;

    return total - watched;
  }



}
