import 'package:cs481_finalproject/BottomNav.dart';
import 'package:cs481_finalproject/MyAccount.dart';
import 'package:cs481_finalproject/ShowWatchListPage.dart';
import 'package:flutter/material.dart';
import 'Statistics.dart';
import 'WatchHistory.dart';
import 'AddShowPage.dart';
import 'dart:ui';
import 'package:cs481_finalproject/CustomWidgets/ColorsAndText.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SideMenu extends StatelessWidget {
  @override

  final String userEmail;
  SideMenu({Key key, this.userEmail}) : super(key: key);

  Widget build(BuildContext context) {
    //Side Menu
    return new Drawer(
      //displays menu items as a list
      child: ListView(
        children: <Widget>[
          //menu header
          DrawerHeader(
            //the drawerHeader is at the top and is blue
            decoration: BoxDecoration(
              color: bColor,
            ),
            //header items placed in a stack, one on top of other
            child: Stack(
              children: <Widget>[
                //widgets for header menu, could contain username,
                //photo, icon, descriptions, etc...
                Align(
                  alignment: Alignment.centerLeft,
                  child: CircleAvatar(
                    radius: 45,
                    backgroundColor: Colors.grey,
                    child: Icon(
                      FontAwesomeIcons.user,
                      color: constWhite,
                      size: 60,
                    ),
                  ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      userEmail, //does not verify length.
                      style: TextStyle(color: constWhite, fontSize: 20.0),
                    ),
                  ),
                Align(
                  alignment: Alignment.centerRight + Alignment(0, 0.3),
                  child: Text(
                    'Registered Dec 2020',
                    style: TextStyle(
                      color: Colors.white70,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight + Alignment(0, 0.8),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: constWhite),
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Text('Verified', style: TextStyle(color: Colors.greenAccent))
                    )
                  )
                )
              ],
            ),
          ),

          //menu list items
          new ListTile(
            leading: Icon(Icons.person),
            title: new Text('My Account', style: TextStyle(
                fontSize: 20.0, color: Colors.black),),
            onTap: () {Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => MyAccountPage(userEmail)));},
          ),

          new ListTile(
            leading: Icon(Icons.history),
            title: new Text('Watch History', style: TextStyle(
                fontSize: 20.0, color: Colors.black),),
            onTap: () {Navigator.push(context,
              MaterialPageRoute(builder: (context) => WatchHistory()),);}, //no implementation
          ),

          new ListTile(
            leading: Icon(Icons.add_to_queue),
            title: new Text('Add Show', style: TextStyle(
                fontSize: 20.0, color: Colors.black),),
            onTap: () {Navigator.push(context,
              MaterialPageRoute(builder: (context) => AddShowPage()),);},
          ),

          new ListTile(
            leading: Icon(Icons.favorite),
            title: new Text('My Watch List', style: TextStyle(
                fontSize: 20.0, color: Colors.black),),
            onTap: () {Navigator.push(context,
              MaterialPageRoute(builder: (context) => WatchListPage()),);},
          ),


          new ListTile(
            leading: Icon(Icons.equalizer),
            title: new Text('My Stats', style: TextStyle(
                fontSize: 20.0, color: Colors.black),),
            onTap: () {
              Navigator.push(context,
                MaterialPageRoute(builder: (context) => Statistics()),);
            },
          ),

          new Divider(color: Colors.black,
              height: 15.0),

          new ListTile(
            leading: Icon(Icons.exit_to_app),
            title: new Text('Log Out', style: TextStyle(
                fontSize: 20.0, color: Colors.black),),
            onTap: () => Navigator.pushReplacementNamed(context, '/'),
          ),
        ],
      ),
    );
  }
}


