import 'package:cs481_finalproject/BottomNav.dart';
import 'package:flutter/material.dart';
import 'Show.dart';
import 'package:cs481_finalproject/CustomWidgets/ColorsAndText.dart';

//Display a few of the most recently watched shows
class WatchHistory extends StatelessWidget {
  //gets the list of Shows
  final List<Show> showsList = Show().getShows();

  @override
  //data table widget and data
  Widget tableData() => DataTable(
    columnSpacing: 40.0,
    columns: <DataColumn>[
      //list of data columns
      DataColumn(
        label: Text(
          "Title",
          style: TextStyle(fontStyle: FontStyle.italic),
        ),
      ),
      DataColumn(
        label: Text(
          "Episode #",
          style: TextStyle(fontStyle: FontStyle.italic),
        ),
      ),
      DataColumn(
        label: Text(
          "Watched On",
          style: TextStyle(fontStyle: FontStyle.italic),
        ),
      ),
    ],
    rows: <DataRow>[
      for (var rowData in showsList)
        DataRow(
          //for each
          cells: <DataCell>[
            DataCell(Text(rowData.showTitle)),
            DataCell(Text(rowData.currentEpisode.toString())),
            DataCell(Text(rowData.showDate)),
          ]
        )
    ],
  );

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Watch History"),
          backgroundColor: bColor,
        ),
        body: Container(
          child: tableData(),
        ));
  }
}
