import 'package:flutter/material.dart';
import 'Show.dart';

//List of photo locations
final List<Show> showsList = Show().getShows();

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: GridView.builder(
        itemBuilder: buildShow,
        itemCount: showsList.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 250 / 400),
      ),
    );
  }
}

Widget buildShow(BuildContext context, int index) {
  //Check if info was pulled from search
  if (showsList[index].manualAdd == true) {
    return new Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      elevation: 10.0,
      //container for the images/movie posters
      child: Container(
        height: 350.0,
        width: 250.0,
        decoration: BoxDecoration(
          image: DecorationImage(
            //adds image from array using index of GridView
            image: FileImage(showsList[index].showImage),
            fit: BoxFit.fill,
          ),
        ),
        //displays the index number of the card, top left
        child: Text(
          (index + 1).toString(),
          style: TextStyle(fontSize: 20.0, color: Colors.white),
        ),
      ),
    );
  } else {
    //If it's from search use network link for image
    return new Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      elevation: 10.0,
      //container for the images/movie posters
      child: Container(
        height: 350.0,
        width: 250.0,
        decoration: BoxDecoration(
          image: DecorationImage(
            //adds image from array using index of GridView
            image: NetworkImage(showsList[index].networkShowImage),
            fit: BoxFit.fill,
          ),
        ),
        //displays the index number of the card, top left
        child: Text(
          (index + 1).toString(),
          style: TextStyle(fontSize: 20.0, color: Colors.white),
        ),
      ),
    );
  }
}
