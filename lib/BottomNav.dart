import 'package:flutter/material.dart';
import 'HomePage.dart';
import 'ShowTab.dart';
import 'package:cs481_finalproject/CustomWidgets/ColorsAndText.dart';
import 'package:cs481_finalproject/Search.dart';
var bColor = darkRed;

class BottomNav extends StatefulWidget {
  @override
  _BottomNavState createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {

  int _currentIndex = 0; //int to keep track of what index
  final List<Widget> _children = //a List for the pages, it gets used for the body property
  [
    HomePage(),  //shows
    ShowTab(),
    APISearchPage(),
    HomePage(),  //TODO trending page
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _children[_currentIndex], // updates the body to be whatever page matches the current index ex) 0 = home page so displays homepage
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          // the bottom navigation bar constructor
          backgroundColor: bColor,
          currentIndex: _currentIndex,
          //sets the currentIndex to be the currentIndex which was initialized as 0
          items: [
            //items are the buttons at the bottom
            BottomNavigationBarItem(
              icon: Icon(Icons.tv, color: Colors.white),
              title: Text('Shows', style: TextStyle(color: Colors.white,),),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.live_tv, color: Colors.white),
              title: Text('Shows List', style: TextStyle(color: Colors.white,),),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search, color: Colors.white),
              title: Text('Search', style: TextStyle(color: Colors.white,),),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.trending_up, color: Colors.white),
              title: Text('Trending', style: TextStyle(color: Colors.white,),),
            ),
          ],
          onTap: (index) {
            //so when the user taps/presses on the button it updates the state to the current index which is whatever button they pressed
            setState(() {
              _currentIndex = index;
            });
          },
        )
    );
  }
}