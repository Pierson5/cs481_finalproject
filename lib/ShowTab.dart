import 'package:cs481_finalproject/BottomNav.dart';
import 'package:flutter/material.dart';
import 'package:cs481_finalproject/main.dart';
//import 'main.dart';
import 'Show.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

//global index to determine which show you're looking at
var currIndex = 0;

class ShowTab extends StatelessWidget {
  //the list that holds all the shows
  final List<Show> showsList = Show().getShows();

  //bool that holds the choice
  bool choice;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //uses listview builder to build the list
        body: ListView.builder(
      //sets the itemcount to how many is in the shows list
      itemCount: showsList.length,
      itemBuilder: buildShow,
    ));
  }

  Widget buildShow(BuildContext context, int index) {
    //Check if pulled from search, if it is use network link
    if (showsList[index].manualAdd == true) {
      return new GestureDetector(
        onTap: () => {
          //uses the current index to build that show's info
          currIndex = index,
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => ShowInfo())),
        },
        child: new Card(
            child: new Column(children: <Widget>[
          new ListTile(
              leading: Container(
                height: 50,
                width: 30,
                //border for the images
                decoration: BoxDecoration(
                    border: Border.all(
                  width: 2,
                )),
                //Show's poster image to whatever index it's in
                child: Image.file(
                  showsList[index].showImage,
                  fit: BoxFit.cover,
                ),
              ),
              title: new Text(
                //sets the show's title to whatever index it's in
                showsList[index].showTitle,
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
              subtitle: new Column(
                  //shows how many total episodes there are for the show
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("\n", style: TextStyle(fontSize: 2.0)),
                    Text(showsList[index].totalEpisodes.toString() +
                        ' Episodes Total'),
                  ]))
        ])),
      );
    }
    //Gesture Detector added to all the shows so you can go to that show's info
    else
      return new GestureDetector(
        onTap: () => {
          //uses the current index to build that show's info
          currIndex = index,
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => ShowInfo())),
        },
        child: new Card(
            child: new Column(children: <Widget>[
          new ListTile(
              leading: Container(
                height: 50,
                width: 30,
                //border for the images
                decoration: BoxDecoration(
                    border: Border.all(
                  width: 2,
                )),
                //Show's poster image to whatever index it's in
                child: Image.network(
                  showsList[index].networkShowImage,
                  fit: BoxFit.cover,
                ),
              ),
              title: new Text(
                //sets the show's title to whatever index it's in
                showsList[index].showTitle,
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
              subtitle: new Column(
                  //shows how many total episodes there are for the show
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("\n", style: TextStyle(fontSize: 2.0)),
                    Text(showsList[index].totalEpisodes.toString() +
                        ' Episodes Total'),
                  ]))
        ])),
      );
  }
}

//enum for the popupmenu
enum MenuOption { edTitle, edImg, edCurr, edTotal, edDescription }

class ShowInfo extends StatelessWidget {
  final List<Show> showsList = Show().getShows();

  @override
  Widget build(BuildContext context) {
    //Check if pulled from search, if it is use network link
    if (showsList[currIndex].manualAdd == true) {
      return new WillPopScope(
          //this makes sure that if you go editing the show's info, the back button
          //when you go back to the show's info takes u to home and not back into the edit page
          onWillPop: () async => false,
          child: Scaffold(
            appBar: AppBar(
                title: Text(showsList[currIndex].showTitle,
                    style: Theme.of(context).textTheme.bodyText1),
                leading: new IconButton(
                    icon: new Icon(Icons.arrow_back),
                    onPressed: () => Navigator.push(context,
                        MaterialPageRoute(builder: (context) => MyHome()))),
                backgroundColor: bColor,
                actions: <Widget>[
                  //the PopupMenu for editing the show's info
                  PopupMenuButton<MenuOption>(
                    onSelected: (MenuOption newValue) {
                      if (newValue == MenuOption.edTitle) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => updateTitle()));
                      } else if (newValue == MenuOption.edImg) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => updateImg()));
                      } else if (newValue == MenuOption.edCurr) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => updateCurrEp()));
                      } else if (newValue == MenuOption.edTotal) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => updateTotalEp()));
                      } else if (newValue == MenuOption.edDescription) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => updateShowDesc()));
                      }
                    },
                    icon: Icon(Icons.edit),
                    itemBuilder: (BuildContext context) {
                      return <PopupMenuEntry<MenuOption>>[
                        PopupMenuItem(
                          child: Text('Edit Title'),
                          value: MenuOption.edTitle,
                        ),
                        PopupMenuItem(
                          child: Text('Edit Show Image'),
                          value: MenuOption.edImg,
                        ),
                        PopupMenuItem(
                          child: Text('Edit Current Episode'),
                          value: MenuOption.edCurr,
                        ),
                        PopupMenuItem(
                          child: Text('Edit Total Episodes'),
                          value: MenuOption.edTotal,
                        ),
                        PopupMenuItem(
                          child: Text('Edit Episode Description'),
                          value: MenuOption.edDescription,
                        ),
                      ];
                    },
                  )
                ]),
            //the container for the whole page, arranged in a column for the info
            body: Container(
                child: Column(children: [
              //Container for the show's image
              Container(
                alignment: Alignment.center,
                height: 350,
                width: 250,
                margin: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: bColor,
                    width: 3,
                  ),
                  //the show's poster image
                  image: DecorationImage(
                    image: FileImage(showsList[currIndex].showImage),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.tv_sharp, color: bColor),
                      title: Text('Title', style: TextStyle(fontWeight: FontWeight.bold)),
                      subtitle: Text(showsList[currIndex].showTitle),
                    ),
                    ListTile(
                      leading: Icon(Icons.format_list_numbered, color: bColor),
                      title: Text('Episodes Watched / Total Episodes', style: TextStyle(fontWeight: FontWeight.bold)),
                      subtitle: Text(showsList[currIndex].currentEpisode.toString() + '/' + showsList[currIndex].totalEpisodes.toString())
                    ),
                    ListTile(
                        leading: Icon(Icons.description, color: bColor),
                        title: Text('Description', style: TextStyle(fontWeight: FontWeight.bold)),
                        subtitle: Text(showsList[currIndex].showDesc)
                    )
              ]
            )
            ])),
          ));
    } else
      return new WillPopScope(
          //this makes sure that if you go editing the show's info, the back button
          //when you go back to the show's info takes u to home and not back into the edit page
          onWillPop: () async => false,
          child: Scaffold(
            appBar: AppBar(
                title: Text(showsList[currIndex].showTitle,
                    style: Theme.of(context).textTheme.bodyText1),
                leading: new IconButton(
                    icon: new Icon(Icons.arrow_back),
                    onPressed: () => Navigator.push(context,
                        MaterialPageRoute(builder: (context) => MyHome()))),
                backgroundColor: bColor,
                actions: <Widget>[
                  //the PopupMenu for editing the show's info
                  PopupMenuButton<MenuOption>(
                    onSelected: (MenuOption newValue) {
                      if (newValue == MenuOption.edTitle) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => updateTitle()));
                      } else if (newValue == MenuOption.edImg) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => updateImg()));
                      } else if (newValue == MenuOption.edCurr) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => updateCurrEp()));
                      } else if (newValue == MenuOption.edTotal) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => updateTotalEp()));
                      } else if (newValue == MenuOption.edDescription) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => updateShowDesc()));
                      }
                    },
                    icon: Icon(Icons.edit),
                    itemBuilder: (BuildContext context) {
                      return <PopupMenuEntry<MenuOption>>[
                        PopupMenuItem(
                          child: Text('Edit Title'),
                          value: MenuOption.edTitle,
                        ),
                        PopupMenuItem(
                          child: Text('Edit Show Image'),
                          value: MenuOption.edImg,
                        ),
                        PopupMenuItem(
                          child: Text('Edit Current Episode'),
                          value: MenuOption.edCurr,
                        ),
                        PopupMenuItem(
                          child: Text('Edit Total Episodes'),
                          value: MenuOption.edTotal,
                        ),
                        PopupMenuItem(
                          child: Text('Edit Episode Description'),
                          value: MenuOption.edDescription,
                        ),
                      ];
                    },
                  )
                ]),
            //the container for the whole page, arranged in a column for the info
            body: Container(
                child: Column(children: [
              //Container for the show's image
              Container(
                alignment: Alignment.center,
                height: 350,
                width: 250,
                margin: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: bColor,
                    width: 3,
                  ),
                  //the show's poster image
                  image: DecorationImage(
                    image: NetworkImage(showsList[currIndex].networkShowImage),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              //Container for the show's info like title, episodes, etc.
              ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.tv_sharp, color: bColor),
                      title: Text('Title', style: TextStyle(fontWeight: FontWeight.bold)),
                      subtitle: Text(showsList[currIndex].showTitle),
                    ),
                    ListTile(
                        leading: Icon(Icons.format_list_numbered, color: bColor),
                        title: Text('Episodes Watched / Total Episodes', style: TextStyle(fontWeight: FontWeight.bold)),
                        subtitle: Text(showsList[currIndex].currentEpisode.toString() + '/' + showsList[currIndex].totalEpisodes.toString())
                    ),
                    ListTile(
                        leading: Icon(Icons.description, color: bColor),
                        title: Text('Description', style: TextStyle(fontWeight: FontWeight.bold)),
                        subtitle: Text(showsList[currIndex].showDesc)
                    )
                  ]
              )
            ])),
          ));
  }
}

//update title page
class updateTitle extends StatelessWidget {
  final updatedShow = Show();
  final titleController = TextEditingController();

  void dispose() {
    titleController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Edit Title"),
          backgroundColor: bColor,
        ),
        body: Column(children: [
          TextFormField(
            controller: titleController,
            decoration: InputDecoration(
                labelText: 'New Title', icon: Icon(Icons.tv, color: bColor)),
          ),
          RaisedButton(
            elevation: 15.0,
            child: Text('Submit'),
            color: bColor,
            textColor: Colors.white,
            onPressed: () {
              updatedShow.updateTitle(currIndex, titleController.text);
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => ShowInfo()));
            },
          )
        ]));
  }
}

//update image page
class updateImg extends StatefulWidget {
  @override
  _updateImgState createState() => _updateImgState();
}

class _updateImgState extends State<updateImg> {
  final updatedShow = Show();

  //File to hold the file and ImagePicker instantiation
  File showImg;
  final _picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Edit Show Image"),
          backgroundColor: bColor,
        ),
        body: Column(children: [
          Container(
            child: Row(children: [
              Container(
                padding: EdgeInsets.only(right: 15.0),
                child: Icon(Icons.image, color: bColor),
              ),
              FlatButton(
                color: bColor,
                child: Text('Add Image'),
                textColor: Colors.white,
                onPressed: () async => _pickImageFromGallery(),
              ),
            ]),
          ),
          RaisedButton(
            elevation: 15.0,
            child: Text('Submit'),
            color: bColor,
            textColor: Colors.white,
            onPressed: () {
              updatedShow.updateShowImg(currIndex, showImg);
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => ShowInfo()));
            },
          )
        ]));
  }

  //this allows the user to pick the image from the gallery
  Future<void> _pickImageFromGallery() async {
    final PickedFile pickedFile =
        await _picker.getImage(source: ImageSource.gallery);
    setState(() {
      this.showImg = File(pickedFile.path);
    });
  }
}

//update current episode page
class updateCurrEp extends StatelessWidget {
  final updatedShow = Show();
  final currEpController = TextEditingController();

  void dispose() {
    currEpController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Edit Current Episode"),
          backgroundColor: bColor,
        ),
        body: Column(children: [
          TextFormField(
            keyboardType: TextInputType.number,
            controller: currEpController,
            decoration: InputDecoration(
                labelText: 'New Current Episode',
                icon: Icon(Icons.tv, color: bColor)),
          ),
          RaisedButton(
            elevation: 15.0,
            child: Text('Submit'),
            color: bColor,
            textColor: Colors.white,
            onPressed: () {
              updatedShow.updateShowCurr(
                  currIndex, int.parse(currEpController.text));
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => ShowInfo()));
            },
          )
        ]));
  }
}

//update total episode page
class updateTotalEp extends StatelessWidget {
  final updatedShow = Show();
  final totalEpController = TextEditingController();

  void dispose() {
    totalEpController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Edit Total Episodes"),
          backgroundColor: bColor,
        ),
        body: Column(children: [
          TextFormField(
            controller: totalEpController,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                labelText: 'New Total Episodes',
                icon: Icon(Icons.tv, color: bColor)),
          ),
          RaisedButton(
            elevation: 15.0,
            child: Text('Submit'),
            color: bColor,
            textColor: Colors.white,
            onPressed: () {
              updatedShow.updateShowEp(
                  currIndex, int.parse(totalEpController.text));
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => ShowInfo()));
            },
          )
        ]));
  }
}

//update the show's description page
class updateShowDesc extends StatelessWidget {
  final updatedShow = Show();
  final showDescController = TextEditingController();

  void dispose() {
    showDescController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Edit Show Description"),
          backgroundColor: bColor,
        ),
        body: Column(children: [
          TextFormField(
            controller: showDescController,
            decoration: InputDecoration(
                labelText: 'New Show Description',
                icon: Icon(Icons.tv, color: bColor)),
          ),
          RaisedButton(
            elevation: 15.0,
            child: Text('Submit'),
            color: bColor,
            textColor: Colors.white,
            onPressed: () {
              updatedShow.updateShowDesc(currIndex, showDescController.text);
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => ShowInfo()));
            },
          )
        ]));
  }
}
