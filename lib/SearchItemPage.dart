import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'API Interaction/APIDirect.dart';
import 'package:cs481_finalproject/BottomNav.dart';
import 'package:cs481_finalproject/Show.dart';

//Label for button
String AddShow;

//Check if the show is added to the list [T=added F=not in]
bool checkShowData(List<Show> toCheck, String showTitle) {
  for (int i = 0; i < toCheck.length; i++)
    if (toCheck[i].showTitle == showTitle) return true;
  return false;
}

class APIitemPage extends StatefulWidget {
  @override
  _APIitemPageState createState() => _APIitemPageState();
}

class _APIitemPageState extends State<APIitemPage> {
  //Grab the show data into an Show object
  Widget grabShowData(AsyncSnapshot snapshot, Show grabShow) {
    grabShow.showTitle = snapshot.data.title;
    grabShow.showDate = snapshot.data.released;
    grabShow.showDesc = snapshot.data.plot;
    grabShow.networkShowImage = snapshot.data.poster;
    grabShow.manualAdd = false;
    grabShow.totalEpisodes = 1;
    grabShow.currentEpisode = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text("Search"),
            backgroundColor: bColor,
            actions: <Widget>[]),
        body: FutureBuilder<ShowData>(
            future: getShowDirect(),
            //Assign the future used, data goes to snapshot
            builder: (context, snapshot) {
              //Check if the connection is done
              if (snapshot.connectionState == ConnectionState.done) {
                //Display error if something goes wrong
                if (snapshot.hasError) {
                  return Text("Error");
                }
                Show grabShow = new Show();
                grabShowData(snapshot, grabShow);
                if (checkShowData(Show.showList, grabShow.showTitle) == true) {
                  AddShow = "Remove Show";
                } else {
                  AddShow = "Add Show";
                }
                //Display the data from the server
                return ListView(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
                      child: Center(
                        child: Text(
                          snapshot.data.title,
                          style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 15.0),
                    Center(child: Image.network(snapshot.data.poster)),
                    SizedBox(height: 15.0),
                    SingleChildScrollView(
                      child: Row(
                        children: [
                          Container(
                            padding: EdgeInsets.all(1),
                            child: Card(
                              child: Container(
                                width: 250,
                                padding: EdgeInsets.all(10),
                                child: Text('Rated : ${snapshot.data.rated}\n'
                                    'Released : ${snapshot.data.released}\n'
                                    'Seasons : ${snapshot.data.totalSeasons}\n'
                                    'Genre : ${snapshot.data.genre}'),
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(1),
                            child: Card(
                              child: Container(
                                width: 250,
                                padding: EdgeInsets.all(10),
                                child: Text(
                                    'Director : ${snapshot.data.director}\n'
                                    'Writer : ${snapshot.data.writer}\n'
                                    'Actors : ${snapshot.data.actors}'),
                              ),
                            ),
                          ),
                        ],
                      ),
                      scrollDirection: Axis.horizontal,
                    ),
                    Card(
                      child: Container(
                        child: Text(
                          snapshot.data.plot,
                          style: TextStyle(color: Colors.white),
                        ),
                        padding: EdgeInsets.all(10),
                        color: Colors.black54,
                      ),
                      shadowColor: Colors.grey.shade700,
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                      child: RaisedButton(
                        onPressed: () {
                          setState(() {
                            if (checkShowData(
                                    Show.showList, snapshot.data.title) ==
                                false) {
                              Show.showList.add(grabShow);
                              AddShow = "Remove Show";
                            } else {
                              Show.showList.removeWhere((item) =>
                                  item.showTitle == grabShow.showTitle);
                              AddShow = "Add Show";
                            }
                          });
                        },
                        child: Text(AddShow, style: TextStyle(fontSize: 20)),
                      ),
                    )
                  ],
                );
              } else
                //If connection is not done, just show loading circle
                return Center(child: CircularProgressIndicator());
            }));
  }
}
