import 'package:cs481_finalproject/LoginScreens/Login.dart';
import 'package:flutter/material.dart';
import 'Show.dart';
import 'BottomNav.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:cs481_finalproject/CustomWidgets/ColorsAndText.dart';
import 'package:cs481_finalproject/BottomNav.dart';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'dart:async';

final newShow = new Show();
String password1 = null;
String password2 = null;

Widget changePassButton = Text("Submit", style: TextStyle(color: Colors.white));

bool checkPass () {
  if (password1 != null && password2 != null && (password1 != password2)) {
    return true;
  }
  return false;
}

class MyAccountPage extends StatefulWidget {
  @override
  final String userEmail;
  const MyAccountPage(this.userEmail);
  _MyAccountPage createState() => _MyAccountPage();
}

class _MyAccountPage extends State<MyAccountPage> {

  bool changePass = false;

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: bColor,
          title: Text('My Account', style: Theme.of(context).textTheme.bodyText1,),
        ),
        body: Builder(
            builder: (context) =>
            ListView(
            children: [
              Padding(
                padding: const EdgeInsets.all(20),
                //alignment: Alignment.center,
                child: CircleAvatar(
                  radius: 45,
                  backgroundColor: Colors.grey,
                  child: Icon(
                    FontAwesomeIcons.user,
                    color: constWhite,
                    size: 60,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Align(
                //alignment: Alignment.center,
                  child: Text(
                    "Username: " + widget.userEmail, //does not verify length.
                    style: TextStyle(color: darkRed, fontSize: 20.0),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Align(
                alignment: Alignment.center,
                child: FlatButton(
                  color: bColor,
                  child: Text('Change password'),
                  textColor: Colors.white,
                  onPressed: () {
                    changePass = true;
                    setState(() {
                      //You can also make changes to your state here.
                    });
                  }
                ),
              ),
              ),
              changePass ?
                  Padding(padding: const EdgeInsets.all(20),
                    child: Align(
                      alignment: Alignment.center,
                      child: TextField(
                        onChanged: (value) => password1 = value,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          hintText: 'Last password'
                        ),
                        style: TextStyle(color: darkRed, fontSize: 20.0),
                      ),
                    ),
              ) : Align(),
              changePass ?
                  Padding(
                      padding: const EdgeInsets.all(20),
                      child: Align(
                        alignment: Alignment.center,
                        child: TextField(
                          onChanged: (value) => password2 = value,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          hintText: 'New password'
                        ),
                        style: TextStyle(color: darkRed, fontSize: 20.0),
                        ),
                      ),
              ) : Align(),
              changePass ?
              Padding(
                padding: const EdgeInsets.all(20),
                child: Align(
                  alignment: Alignment.center,
                  child: FlatButton(
                      color: bColor,
                      child: changePassButton,
                      onPressed: () {
                        if (checkPass()) {
                          setState(() {
                            changePassButton = CircularProgressIndicator(
                              backgroundColor: Colors.blueGrey,
                            );
                          });
                          Timer(Duration(seconds: loginDelay), () {
                            //Change icon back to login text
                            setState(() {
                              changePassButton =
                                  Text("Submit",
                                      style: TextStyle(color: Colors.white));
                            });
                            //goto home Page
                            Navigator.push(context,
                              MaterialPageRoute(
                                  builder: (context) => LoginScreen()),);
                          });
                        } else {
                          final snackBar = SnackBar(content: Text('Both passwords must be different and not null.'));
                          Scaffold.of(context).showSnackBar(snackBar);
                        }
                      }
                  ),
                ),
              ) : Align()
            ]
        )
        ) );
  }
}



