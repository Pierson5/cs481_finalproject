import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cs481_finalproject/LoginScreens/Login.dart';
import 'package:cs481_finalproject/LoginScreens/NewAcc.dart';
import 'package:cs481_finalproject/BottomNav.dart';
import 'package:cs481_finalproject/SideMenu.dart';
import 'package:cs481_finalproject/SettingsButton.dart';
import 'CustomWidgets/globals.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MovieApp',
      //https://flutter.dev/docs/cookbook/design/themes
      theme: ThemeData(
        textTheme:
        GoogleFonts.josefinSansTextTheme(Theme.of(context).textTheme),
        primarySwatch: Colors.red,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),

      //Navigator/Routes
      //switches between screens based on their names and
      //GestureDetector / onTap() function on buttons.
      initialRoute: '/',
      routes: {
        '/': (context) => LoginScreen(),
        'CreateNewAccount': (context) => CreateNewAccount(),
        'HomePage' : (context) => MyHome(),
      },
    );
  }
}

class MyHome extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    //get userEmail from login page
    if(logInClicked){
      userEmail = ModalRoute.of(context).settings.arguments;
      logInClicked = false; //only update when login button clicked
    }

    return Scaffold(
          appBar: AppBar(
            centerTitle: true,
              title: Text('Shows'),
              backgroundColor: bColor,
              actions: <Widget>[
                PopupMenu()
              ]
          ),

          //BottomNavBar
          body: new Stack(
              fit: StackFit.expand,
              children: [
                BottomNav(),
              ]
          ),

          //sideMenu
          drawer: SideMenu(userEmail: userEmail)
        );
  }
} //MyApp
