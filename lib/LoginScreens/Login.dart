import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:cs481_finalproject/CustomWidgets/ColorsAndText.dart';
import 'package:cs481_finalproject/CustomWidgets/BGImageSettings.dart';
import 'package:cs481_finalproject/CustomWidgets/TextInputSettings.dart';
import 'package:cs481_finalproject/CustomWidgets/PasswordInputSettings.dart';
import 'dart:async';
import 'package:cs481_finalproject/CustomWidgets/globals.dart';

Widget loginButton = Text("Login", style: TextStyle(fontSize: 25));
int loginDelay = 3;
var _email;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _textInputController = TextEditingController();
  final _passwordKey = GlobalKey<FormState>();
  final _textKey = GlobalKey<FormState>();
  bool _visible = true;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        //from BGImageSettings.dart
        BackgroundImage(
          imageLocation: 'images/1.jpg',
        ),

        Scaffold(
          backgroundColor: Colors.transparent,
          body: Column(
            children: [
              Flexible(
                child: Center(
                  child: Text(
                    'TV Tracker',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 60,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  AnimatedOpacity(
                    opacity: _visible ? 1.0 : 0.0,
                    duration: Duration(milliseconds: 500),
                    child: TextInputField(
                      icon: FontAwesomeIcons.envelope,
                      hint: 'Email',
                      inputType: TextInputType.emailAddress,
                      inputAction: TextInputAction.next,
                      controller: _textInputController,
                      formKey: _textKey,
                    ),
                  ),
                  AnimatedOpacity(
                    opacity: _visible ? 1.0 : 0.0,
                    duration: Duration(milliseconds: 300),
                    child: PasswordInput(
                      icon: FontAwesomeIcons.lock,
                      hint: 'Password',
                      inputAction: TextInputAction.done,
                      controller: _passwordController,
                      formKey: _passwordKey,
                    ),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Container(
                    width: 300,
                    height: 50,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: BorderSide(color: darkRed),
                      ),
                      onPressed: () {
                        if(_textInputController.text.isEmpty){
                          _textKey.currentState.validate();
                        }
                        if(_passwordController.text.isEmpty){
                          _passwordKey.currentState.validate();
                        }
                        else{
                          logInClicked = true;
                          //Change the login text into a loading icon
                          setState(() {
                            _email = _textInputController.text;
                            loginButton = CircularProgressIndicator(
                              backgroundColor: Colors.blueGrey,
                            );
                            _visible = !_visible;
                          });
                          //Since there is no actual login, delay the next page
                          Timer(Duration(seconds: loginDelay), () {
                            //Change icon back to login text
                            setState(() {
                              loginButton =
                                  Text("Login", style: TextStyle(fontSize: 25));
                              _visible = !_visible;
                            });
                            //goto home Page
                            Navigator.pushReplacementNamed(context, 'HomePage', arguments: _email);
                          });
                        };
                      },
                      color: darkRed,
                      textColor: Colors.white,
                      child: loginButton,
                    ),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                ],
              ),
              AnimatedOpacity(
                opacity: _visible ? 1.0 : 0.0,
                duration: Duration(milliseconds: 500),
                child: GestureDetector(
                  onTap: () => Navigator.pushNamed(context, 'CreateNewAccount'),
                  child: Container(
                    child: Text(
                      'Create New Account',
                      style: TextBodyStyle,
                    ),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(width: 1, color: constWhite))),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        )
      ],
    );
  }
}
