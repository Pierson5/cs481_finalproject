import 'package:cs481_finalproject/BottomNav.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'Show.dart';

final Show showInfo = Show();

class Statistics extends StatelessWidget {
  //variables for statistics (with test values)
  //Total num shows watched * 30minutes (API limitation)
  static int watchedInMinutes = 3628;
  //input how many episodes complete * length of episodes in minutes
  final timeWatched = Duration(minutes: watchedInMinutes);



  //function to convert minutes to days/hours/minutes
  String _printDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String threeDigits(int n) => n.toString().padLeft(3, "0");
    String twoDigitHours = twoDigits(duration.inHours.remainder(24));
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    return "${threeDigits(duration.inDays)} Days $twoDigitHours Hours $twoDigitMinutes Minutes";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Statistics"),
        backgroundColor: bColor,
      ),
      body: ListView(
          children: <Widget>[
            Card(
              child: Container(
                decoration: new BoxDecoration(
                  boxShadow: [
                    new BoxShadow(
                      color: bColor,
                    ),
                  ],
                ),
                alignment: Alignment.center,
                padding: EdgeInsets.all(15.0),
                child: Column(
                    children: <Widget>[
                      Icon(FontAwesomeIcons.tv, color: Colors.white, size: 50),
                      SizedBox(height: 10),
                      Text("You have a total of", style: TextStyle(fontSize: 20, color: Colors.white)),
                      SizedBox(height: 20),
                      Text(showInfo.getTotalShows().toString(), style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold, color: Colors.white)),
                      Text("Television shows", style: TextStyle(fontSize: 20, color: Colors.white)),
                      SizedBox(height: 10),
                    ]
                ),
              ),
            ),

            Card(
              child: Container(
                decoration: new BoxDecoration(
                  boxShadow: [
                    new BoxShadow(
                      color: bColor,
                    ),
                  ],
                ),
                alignment: Alignment.center,
                padding: EdgeInsets.all(15.0),
                child: Column(
                    children: <Widget>[
                      Icon(FontAwesomeIcons.eye, color: Colors.white, size: 50),
                      SizedBox(height: 10),
                      Text("You have watched a total of", style: TextStyle(fontSize: 20, color: Colors.white)),
                      SizedBox(height: 20),
                      Text(showInfo.getTotalEpWatched().toString(), style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold, color: Colors.white)),
                      Text("Episodes", style: TextStyle(fontSize: 20, color: Colors.white)),
                      SizedBox(height: 10),
                    ]
                ),
              ),
            ),


            Card(
              child: Container(
                decoration: new BoxDecoration(
                  boxShadow: [
                    new BoxShadow(
                      color: bColor,
                    ),
                  ],
                ),
                alignment: Alignment.center,
                padding: EdgeInsets.all(15.0),
                  child: Column(
                    children: <Widget>[
                      Icon(FontAwesomeIcons.eyeSlash, color: Colors.white, size: 50),
                      SizedBox(height: 10),
                      Text("You have not seen", style: TextStyle(fontSize: 20, color: Colors.white)),
                      SizedBox(height: 20),
                      Text(showInfo.getTotalEpUnwatched().toString(), style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold, color: Colors.white)),
                      Text("Episodes from your shows", style: TextStyle(fontSize: 20, color: Colors.white)),
                      SizedBox(height: 10),
                    ]
                ),
              ),
            ),


            Card(
              child: Container(
                decoration: new BoxDecoration(
                boxShadow: [
                  new BoxShadow(
                    color: bColor,
                  ),
                ],
              ),
                alignment: Alignment.center,
                padding: EdgeInsets.all(15.0),
                child: Column(
                    children: <Widget>[
                      Icon(FontAwesomeIcons.clock, color: Colors.white, size: 50),
                      SizedBox(height: 10),
                      Text("Total time spent watching shows", style: TextStyle(fontSize: 20, color: Colors.white)),
                      SizedBox(height: 20),
                      Text(watchedInMinutes.toString() + " Minutes", style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white)),
                      SizedBox(height: 10),
                      Text("Which is", style: TextStyle(fontSize: 20, color: Colors.white)),
                      SizedBox(height: 10),
                      Text(_printDuration(timeWatched), style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.white)),
                      SizedBox(height: 10),
                    ]
                ),
              ),
            ),
          ],
        )
    );   //),
   // );
  }
}