import 'package:flutter/material.dart';

//custom widget for BackgroundImage
//https://www.youtube.com/watch?v=CyKFCLtOb_s
class BackgroundImage extends StatelessWidget {
  //define parameters
  const BackgroundImage({
    Key key,
    @required this.imageLocation,
  }) : super(key: key);

  final String imageLocation;
  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (rect) => LinearGradient(
        begin: Alignment.bottomCenter,
        end: Alignment.center,
        colors: <Color>[Colors.black, Colors.transparent],
      ).createShader(rect),
      blendMode: BlendMode.darken,
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(imageLocation),
            fit: BoxFit.cover,
            //darken image, make text 'pop'
            colorFilter: ColorFilter.mode(Colors.black38, BlendMode.darken),
          ),
        ),
      ),
    );
  }
}