import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//constants
const Color constWhite = Colors.white;
const Color customBlue = Color(0xff5663ff);
const Color darkRed = Color(0xff7A1200);


//body style
const TextStyle TextBodyStyle =
TextStyle(fontSize: 22, color: Colors.white, height: 1.5);