import 'package:flutter/material.dart';
import 'package:cs481_finalproject/CustomWidgets/ColorsAndText.dart';

//custom widget for Button
//https://www.youtube.com/watch?v=CyKFCLtOb_s
class RoundedButton extends StatelessWidget {
  const RoundedButton({
    Key key,
    @required this.buttonName,
  }) : super(key: key);

  final String buttonName;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.08,
      width: size.width * 0.8,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: darkRed,
      ),
      child: FlatButton(
        onPressed: () {},
        child: Text(
          buttonName,
          style: TextBodyStyle.copyWith(fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}