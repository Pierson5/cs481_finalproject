import 'package:flutter/material.dart';
import 'package:cs481_finalproject/CustomWidgets/ColorsAndText.dart';

//custom widget for text input field
//https://www.youtube.com/watch?v=CyKFCLtOb_s

class TextInputField extends StatelessWidget {
  //Input field of textInput
  const TextInputField({
    Key key,
    @required this.icon,
    @required this.hint,
    this.inputType,
    this.inputAction,
    this.controller,
    this.formKey,
  }) : super(key: key);

  final IconData icon;
  final String hint;
  final TextInputType inputType;
  final TextInputAction inputAction;
  final TextEditingController controller;
  final GlobalKey formKey;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Container(
        height: size.height * 0.08,
        width: size.width * 0.8,
        decoration: BoxDecoration(
          color: Colors.grey[500].withOpacity(0.5),
          borderRadius: BorderRadius.circular(16),
        ),
        child: Center(
          child: Form(
            key: formKey,
            child: TextFormField(
              controller: controller, //used to verify login
              validator: (val) => val.isEmpty ? "Field Cannot be Empty!" : null,
              decoration: InputDecoration(
                errorStyle: TextStyle(color: constWhite),
                border: InputBorder.none,
                prefixIcon: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Icon(
                    icon,
                    size: 28,
                    color: constWhite,
                  ),
                ),
                hintText: hint,
                hintStyle: TextBodyStyle,
              ),
              style: TextBodyStyle,
              keyboardType: inputType,
              textInputAction: inputAction,
            ),
          ),
        ),
      ),
    );
  }
}