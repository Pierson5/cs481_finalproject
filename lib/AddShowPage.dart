import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'Show.dart';
import 'main.dart';
import 'BottomNav.dart';

final newShow = new Show();

class AddShowPage extends StatefulWidget {
  @override
  _AddShowPageState createState() => _AddShowPageState();
}

class _AddShowPageState extends State<AddShowPage> {
  //controllers that handle getting the input from the textfields
  final titleController = TextEditingController();
  final episodeController = TextEditingController();
  final currEpisodeController = TextEditingController();
  final descController = TextEditingController();

  //global key for the form where it checks its state
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  //File to hold the file and ImagePicker instantiation
  File showImg;
  final _picker = ImagePicker();

  //Image Confirmation variable
  var imageConfirm = false;

  //void function to use validated for the formKey which
  //runs the validator functions in the form to see if the all the validator functions check out
  void validated(){
    if(formKey.currentState.validate()){
      //Navigator pushes the page replacement to the appTabs which gets rid of the form if the form is valid
      DateTime now = DateTime.now();
      String formattedDate = DateFormat('MMM d, y').format(now);
      newShow.addShow(titleController.text, int.parse(episodeController.text), int.parse(currEpisodeController.text), showImg, descController.text, formattedDate, true);
      Navigator.push(context, MaterialPageRoute(builder: (context) => MyHome()));
    }
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    titleController.dispose();
    episodeController.dispose();
    currEpisodeController.dispose();
    descController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: bColor,
          title: Text('Add Show', style: Theme.of(context).textTheme.bodyText1,),
        ),
        body: Builder(
            builder: (context) =>
            ListView(
            children: [
              Form(
                //sets the key for the form to formKey
                  key: formKey,
                  child: Column(children: <Widget> [
                    //add Show's title
                    TextFormField(
                      //sets controller to nameController which gets the textfield input
                        controller: titleController,
                        //decoration used to show a hint for what to put
                        decoration: InputDecoration(
                            labelText: 'Title',
                            icon: Icon(Icons.tv, color: bColor) ),
                        //validator checks the input for the textfield and sees if it's empty
                        //if it is empty, when the user presses submit the validator msg shows
                        validator: (String value) {
                          if(value.isEmpty)
                          {
                            return "Invalid: Cannot be empty.";
                          }else {
                            return null;
                          }
                        }
                    ),
                    TextFormField(
                      //add Show's total ep
                        controller: episodeController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: 'Total Episodes',
                          icon: Icon(Icons.format_list_numbered, color: bColor),
                        ),
                        validator: (String value) {
                          RegExp exp = new RegExp(r'^(([0-9]*))$');
                          if(value.isEmpty)
                          {
                            return "Invalid: Cannot be empty.";
                          }else if (exp.hasMatch(value)) {
                            return null;
                          } else
                            return "Invalid: Not a number.";
                        }
                    ),
                    TextFormField(
                      //add Show's current ep
                        controller: currEpisodeController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: 'Current Episode You Are On',
                          icon: Icon(Icons.live_tv, color: bColor),
                        ),
                        validator: (String value) {
                          RegExp exp = new RegExp(r'^(([0-9]*))$');
                          if(value.isEmpty)
                          {
                            return "Invalid: Cannot be empty.";
                          }else if (exp.hasMatch(value)) {
                            if(int.parse(currEpisodeController.text) > int.parse(episodeController.text))
                            {
                              return "Invalid: You cannot have more than the total episodes.";
                            }
                            else
                            {
                              return null;
                            }
                          } else
                            return "Invalid: Not a number.";
                        }
                    ),
                    Container(
                      //add Show's image
                      child: Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 15.0),
                              child: Icon(Icons.image, color: bColor),
                            ),
                            FlatButton(
                              color: bColor,
                              child: Text('Add Image'),
                              textColor: Colors.white,
                              onPressed: () async =>
                                  _pickImageFromGallery(),
                            ),
                          ]
                      ),
                    ),
                    TextFormField(
                      //add Show's description
                      controller: descController,
                      keyboardType: TextInputType.multiline,
                      minLines: 1,
                      maxLines: 5,
                      //decoration used to show a hint for what to put
                      decoration: InputDecoration(
                          labelText: 'Show Description',
                          icon: Icon(Icons.edit, color: bColor) ),
                    ),
                    RaisedButton(
                      elevation: 15.0,
                      child: Text('Submit'),
                      textColor: Colors.white,
                      color: bColor,
                      onPressed: () {
                        //when the button is pressed, calls the validated function to check the form's validity
                        //then sets the global string variables to what was put in the form
                        if (imageConfirm == true)
                          validated();
                        else
                          showSnackBar(context);
                      },
                    ),
                  ])
              ),
            ]
        )
        ) );

  }

  void showSnackBar(BuildContext context) {
    //the function to show the snackBar
    SnackBar theSnackBar = new SnackBar(
      content: Text('You must add an image'),
      action: SnackBarAction(
        label: 'Hide',
        onPressed: () {
        },
      ),
      duration: const Duration(seconds: 3),
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
    );
    //Snackbar always paired to the scaffold
    Scaffold.of(context).showSnackBar(theSnackBar);
  }

  //lets the user pick an image from their phone's gallery
  Future<void> _pickImageFromGallery() async{
    final PickedFile pickedFile =
    await _picker.getImage(source: ImageSource.gallery);
    setState(() {
      this.showImg = File(pickedFile.path);
      imageConfirm = true;
    });
  }
}



