import 'package:flutter/material.dart';
import 'package:cs481_finalproject/CustomWidgets/ColorsAndText.dart';
import 'package:cs481_finalproject/BottomNav.dart';

//List for popup menu options
enum MenuOption {Settings, LogOut}

bool  showAllTypesSearch = false;

class PopupMenu extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return IconButton(
      icon: Icon(Icons.settings),
      color: Colors.white,
      onPressed: () {
        navigateToSettings(context);
      },
    );
  }
  Future navigateToSettings(context) async {
    Navigator.push(context, MaterialPageRoute(builder: (context) => SettingsPage()));
  }
}

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  MaterialColor backgroundColor;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        leading: Icon(Icons.settings),
        title: Text('Settings'),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () => Navigator.pushReplacementNamed(context, 'HomePage'),
          )
        ],
        backgroundColor: bColor,
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.color_lens),
            title: Text('Theme Color'),
            trailing: DropdownButton(
                value: bColor,
                items: [
                  DropdownMenuItem(
                    child: Text("Red"),
                    value: darkRed,
                  ),
                  DropdownMenuItem(
                    child: Text("Orange"),
                    value: Colors.orange,
                  ),
                  DropdownMenuItem(
                    child: Text("Green"),
                    value: Colors.green,
                  ),
                  DropdownMenuItem(
                    child: Text("Blue"),
                    value: Colors.blue,
                  ),
                  DropdownMenuItem(
                    child: Text("Purple"),
                    value: Colors.purple,
                  )
                ],
                onChanged: (value) {
                  setState(() {
                    bColor = value;
                  });
                }),
          ),
          ListTile(
            leading: Icon(Icons.remove_red_eye_rounded),
            title: Text('Show all search results'),
            trailing: DropdownButton(
                value: showAllTypesSearch,
                items: [
                  DropdownMenuItem(
                    child: Text("Yes"),
                    value: true,
                  ),
                  DropdownMenuItem(
                    child: Text("No"),
                    value: false,
                  ),
                ],
                onChanged: (value) {
                  setState(() {
                    showAllTypesSearch = value;
                  });
                }),
          ),
          ListTile(
              leading: Text('Share The App!'),
              trailing: SelectableText('https://play.google.com/store/\napps/details?id=com.mcdonalds.app')
          ),
          ListTile(
              leading: CircleAvatar(
                  backgroundImage: AssetImage("images/instagram.jpg"),
              ),
              title: Text('Follow Us'),
              subtitle: SelectableText("https://www.instagram.com/mcdonalds/?hl=en")
          ),
          ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("images/twitter.jpg"),
              ),
              title: Text('Follow Us'),
              subtitle: SelectableText("https://twitter.com/McDonalds?ref_src=\ntwsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor")
          ),
          ListTile(
              title: Text('Submit Feedback'),
              trailing: SelectableText("huber031@cougars.csusm.edu")
          ),
          ListTile(
              title: Text('Version Number:                                  '
                  '                          .01'),
          ),
        ],
      ),
    );
  }
  void backToMainPage(context) {
    Navigator.pop(context);
  }
}