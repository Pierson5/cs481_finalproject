import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'HomePage.dart';
import 'Show.dart';
import 'main.dart';

final newShow = new Show();

class WatchListPage extends StatefulWidget {
  @override
  _WatchListState createState() => _WatchListState();
}

class _WatchListState extends State<WatchListPage> {
  //controllers that handle getting the input from the textfields
  final titleController = TextEditingController();
  final episodeController = TextEditingController();
  final currEpisodeController = TextEditingController();
  final descController = TextEditingController();

  //global key for the form where it checks its state
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  //File to hold the file and ImagePicker instantiation
  File showImg;
  final _picker = ImagePicker();

  //Image Confirmation variable
  var imageConfirm = false;

  //void function to use validated for the formKey which
  //runs the validator functions in the form to see if the all the validator functions check out
  void validated(){
    if(formKey.currentState.validate()){
      //Navigator pushes the page replacement to the appTabs which gets rid of the form if the form is valid
      DateTime now = DateTime.now();
      String formattedDate = DateFormat('MMM d, y').format(now);
      newShow.addShow(titleController.text, int.parse(episodeController.text), int.parse(currEpisodeController.text), showImg, descController.text, formattedDate, true);
      Navigator.push(context, MaterialPageRoute(builder: (context) => MyHome()));
    }
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    titleController.dispose();
    episodeController.dispose();
    currEpisodeController.dispose();
    descController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return new Scaffold(
      body: GridView.builder(
        itemBuilder: buildShow,
        itemCount: showsList.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 250 / 400),
      ),
    );
  }

  void showSnackBar(BuildContext context) {
    //the function to show the snackBar
    SnackBar theSnackBar = new SnackBar(
      content: Text('You must add an image'),
      action: SnackBarAction(
        label: 'Hide',
        onPressed: () {
        },
      ),
      duration: const Duration(seconds: 3),
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
    );
    //Snackbar always paired to the scaffold
    Scaffold.of(context).showSnackBar(theSnackBar);
  }

  //lets the user pick an image from their phone's gallery
  Future<void> _pickImageFromGallery() async{
    final PickedFile pickedFile =
    await _picker.getImage(source: ImageSource.gallery);
    setState(() {
      this.showImg = File(pickedFile.path);
      imageConfirm = true;
    });
  }
}



